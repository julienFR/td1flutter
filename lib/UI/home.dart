import 'package:flutter/material.dart';
import 'package:quizzapp20222023/UI/myButtons.dart';

import '../model/question.dart';

class MyWidget extends StatefulWidget {
  final Color color;
  final double myTextSize;

  const MyWidget(this.color, this.myTextSize);

  @override
  State<MyWidget> createState() => _MyWidgetState();
}

class _MyWidgetState extends State<MyWidget> {
  //final String message;
  int _currentQuestion = 0;
  late BuildContext context;

  final List _questions = [
    Question.name(
        "The question number 1 is a very long question and her answer is true.",
        true,
        "flag.png"),
    Question.name("The question number 2 is true again.", true, "img.png"),
    Question.name("The question number 3 is false.", false, "img.png"),
    Question.name("The question number 4 is false again.", false, "flag.png"),
    Question.name("The question number 5 is true.", true, "flag.png"),
    Question.name("The question number 6 is true again.", true, "img.png"),
  ];

  @override
  Widget build(BuildContext context) {
    /*final ButtonStyle myButtonStyle = ElevatedButton.styleFrom(
        backgroundColor: Colors.blueGrey.shade900,
        padding: const EdgeInsets.symmetric(horizontal: 20),
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20)),
        ));
    */
    this.context = context;
    return Scaffold(
        appBar: AppBar(
          title: const Text("Quizz App"),
          centerTitle: true,
          backgroundColor: Colors.lightBlue,
        ),
        backgroundColor: widget.color,
        body:
        NotificationListener<IndexChanged>(
          child:Center(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
              Image.asset(
                'images/${_questions[_currentQuestion].image}',
                width: 250,
                height: 180,
              ),
              Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Container(
                      decoration: BoxDecoration(
                          color: Colors.transparent,
                          borderRadius: BorderRadius.circular(15),
                          border: Border.all(
                              color: Colors.black, style: BorderStyle.solid)),
                      //height:120.0,
                      child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Text(
                            //message,
                            _questions[_currentQuestion].questionText,
                            textDirection: TextDirection.ltr,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontStyle: FontStyle.italic,
                              fontSize: widget.myTextSize,
                            ),
                          )))),
              Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
                MyIconButton(myIcon: Icons.arrow_back, value: -1),
               /* ElevatedButton(
                  onPressed: () => _previousQuestion(),
                  //style: myButtonStyle,
                  child: const Icon(
                    Icons.arrow_back,
                    color: Colors.white,
                  ),
                ),*/
                MyTextButton(
                    myText: "TRUE", myValue: true, returnValue: _handleValue),
                /*ElevatedButton(
                  onPressed: () => _checkAnswer(true,context),
                  //style: myButtonStyle,
                  child:
                      const Text("TRUE", style: TextStyle(color: Colors.white)),
                ),*/
                MyTextButton(
                    myText: "FALSE", myValue: false, returnValue: _handleValue),
                /*ElevatedButton(
                  onPressed: () => _checkAnswer(false,context),
                  //style: myButtonStyle,
                  child: const Text("FALSE",
                      style: TextStyle(color: Colors.white)),
                ),*/
                MyIconButton(myIcon: Icons.arrow_forward, value: 1),
               /* ElevatedButton(
                  onPressed: () => _nextQuestion(),
                  //style: myButtonStyle,
                  child: const Icon(
                    Icons.arrow_forward,
                    color: Colors.white,
                  ),
                ),*/
              ])
            ])),
        onNotification: (n){
            _changeQuestion(n.val);
            return true;
        },),

    );
  }

  _previousQuestion() {
    setState(() {
      _currentQuestion = (_currentQuestion - 1) % _questions.length;
    });
  }

  _nextQuestion() {
    setState(() {
      _currentQuestion = (_currentQuestion + 1) % _questions.length;
    });
  }

  _changeQuestion(int n){
    setState(() {
      _currentQuestion = (_currentQuestion + n) % _questions.length;
    });
  }

  void _handleValue(bool value) {
    debugPrint(value.toString());
    if (value == _questions[_currentQuestion].isCorrect) {
      debugPrint("good");
      const mySnackBar = SnackBar(
        content: Text("GOOD ANSWER!!!", style: TextStyle(fontSize: 20)),
        duration: Duration(milliseconds: 500),
        backgroundColor: Colors.lightGreen,
        width: 180.0, // Width of the SnackBar.
        padding: EdgeInsets.symmetric(
          horizontal: 8.0, // Inner padding for SnackBar content.
        ),
        behavior: SnackBarBehavior.floating,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20)),
        ),
      );
      ScaffoldMessenger.of(context).showSnackBar(mySnackBar);
    } else {
      debugPrint("bad");
      const mySnackBar = SnackBar(
        content: Text(
          "BAD ANSWER!!!",
          style: TextStyle(fontSize: 20),
        ),
        duration: Duration(milliseconds: 500),
        backgroundColor: Colors.red,
        width: 180.0, // Width of the SnackBar.
        padding: EdgeInsets.symmetric(
          horizontal: 8.0, // Inner padding for SnackBar content.
        ),
        behavior: SnackBarBehavior.floating,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20)),
        ),
      );
      ScaffoldMessenger.of(context).showSnackBar(mySnackBar);
    }
    _changeQuestion(1);
  }
/*
  _checkAnswer(bool choice, BuildContext context) {
    if (choice == _questions[_currentQuestion].isCorrect){
      debugPrint("good");
      const mySnackBar = SnackBar(
        content: Text("GOOD ANSWER!!!",style: TextStyle(fontSize: 20)),
        duration: Duration(milliseconds: 500),
        backgroundColor: Colors.lightGreen,
        width: 180.0, // Width of the SnackBar.
        padding: EdgeInsets.symmetric(
          horizontal: 8.0, // Inner padding for SnackBar content.
        ),
        behavior: SnackBarBehavior.floating,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20)),
        ),);
      ScaffoldMessenger.of(context).showSnackBar(mySnackBar);
    }else{
      debugPrint("bad");
      const mySnackBar = SnackBar(
        content: Text("BAD ANSWER!!!",style: TextStyle(fontSize: 20),),
        duration: Duration(milliseconds: 500),
        backgroundColor: Colors.red,
        width: 180.0, // Width of the SnackBar.
        padding: EdgeInsets.symmetric(
          horizontal: 8.0, // Inner padding for SnackBar content.
        ),
        behavior: SnackBarBehavior.floating,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20)),
        ),);
      ScaffoldMessenger.of(context).showSnackBar(mySnackBar);
    }
    _nextQuestion();
  }
 */
}
